﻿using Microsoft.EntityFrameworkCore;
using MyWishMarket.EnityFramework.DBOptions;
using Volodin_Hw_2.EnityFramework.Business;

internal class Program
{
    private static void Main(string[] args)
    {
        EntityDataLayer entityDataLayer = new EntityDataLayer();
        var items = entityDataLayer.GetAllItems().ToList();
        var users = entityDataLayer.GetAllUsers().ToList();
        var userItems = entityDataLayer.GetAllUserItems().ToList();

        foreach (var item in items)
        {
            Console.WriteLine($"{item.Id} {item.Name} {item.Price}");
        }
        
        Console.WriteLine(Environment.NewLine);
        
        foreach (var user in users)
        {
            Console.WriteLine($"{user.Id} {user.Name} {user.Email}");
        }

        Console.WriteLine(Environment.NewLine);

        foreach (var userItem in userItems)
        {
            Console.WriteLine($"{userItem.ItemId} {userItem.UserId}");
        }

        Console.WriteLine("Press 1 to add new user;");
        Console.WriteLine("Press 2 to add new item;");
        Console.WriteLine("Press anything to exit.");
        var key = Console.ReadKey(true).KeyChar;
        switch (key)
        {
            case '1':
                entityDataLayer.AddUser();
                break;
            case '2':
                entityDataLayer.AddItem();
                break;
            default:
                break;
        }
    }
}
