﻿using Microsoft.EntityFrameworkCore;
using MyWishMarket.EnityFramework.DBOptions;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Volodin_Hw_2.EnityFramework.Business
{
    public class EntityDataLayer
    {
        public DataContext Db { get; set; }

        /// <summary>
        /// Основной конструктор
        /// </summary>
        public EntityDataLayer()
        {
            Db = new DataContext();
            Db.Database.SetCommandTimeout(120);
        }

        public List<Items> GetAllItems()
        {
            return Db.Item.Select(x => x).OrderBy(x => x.Id).ToList();
        }

        public List<Users> GetAllUsers()
        {
            return Db.User.Select(x => x).OrderBy(x => x.Id).ToList();
        }

        public List<UserItems> GetAllUserItems()
        {
            return Db.UserItem.Select(x => x).ToList();
        }

        internal void AddUser()
        {
            Console.Write("Введите имя пользователя: ");
            var name = Console.ReadLine();
            Console.Write("Введите адрес почты: ");
            var email = Console.ReadLine();
            var newUser = new Users()
            {
                Name = name,
                Email = email
            };
            Db.User.Add(newUser);
            Db.SaveChanges();
            Console.Write("Данные успешно сохранены!");
        }

        internal void AddItem()
        {
            Console.Write("Введите название товара: ");
            var name = Console.ReadLine();
            Console.Write("Введите цену товара: ");
            var price = Decimal.Parse(Console.ReadLine(), CultureInfo.GetCultureInfo("en-US"));
            var newItem = new Items()
            {
                Name = name,
                Price = price
            };
            Db.Item.Add(newItem);
            Db.SaveChanges();
            Console.Write("Данные успешно сохранены!");
        }
    }
}