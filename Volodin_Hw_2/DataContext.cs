﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyWishMarket.EnityFramework.DBOptions
{
    public class DataContext : DbContext
    {
        public DataContext() : base() { }

        public DbSet<Items> Item { get; set; }
        public DbSet<UserItems> UserItem { get; set; }
        public DbSet<Users> User { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["DBConnection"].ConnectionString;
            optionsBuilder.UseNpgsql(connectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Users>().ToTable("users");
            modelBuilder.Entity<Items>().ToTable("items");
            modelBuilder.Entity<UserItems>().ToTable("user_items");

            modelBuilder.Entity<Users>(entity =>
            {
                entity.HasKey(e => e.Id);

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Name).HasColumnName("name");

                entity.Property(e => e.Email).HasMaxLength(50).HasColumnName("email");
            });

            modelBuilder.Entity<Items>(entity =>
            {
                entity.HasKey(e => e.Id);

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Name).HasColumnName("name");

                entity.Property(e => e.Price).HasMaxLength(50).HasColumnName("price");
            });

            modelBuilder.Entity<UserItems>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.ItemId).HasColumnName("item_id");

                entity.Property(e => e.UserId).HasMaxLength(50).HasColumnName("user_id");
            });

        }
    }
}
