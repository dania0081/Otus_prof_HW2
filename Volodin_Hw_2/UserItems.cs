﻿namespace MyWishMarket.EnityFramework.DBOptions
{
    public class UserItems
    {
        /// <summary>
        /// Идентификатор пользователя.
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Идентификатор товара.
        /// </summary>
        public int ItemId { get; set; }
    }
}